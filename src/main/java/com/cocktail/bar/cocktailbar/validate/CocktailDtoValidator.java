package com.cocktail.bar.cocktailbar.validate;

import com.cocktail.bar.cocktailbar.model.enums.TypeOfCocktail;
import dto.CocktailDto;
import jdk.jfr.Category;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.naming.Binding;
import java.util.Locale;

@Component
public class CocktailDtoValidator {
    public void validate(CocktailDto cocktailDto, BindingResult bindingResult) {
        validateName(cocktailDto, bindingResult);
        validatePrice(cocktailDto, bindingResult);
        validateIngredients(cocktailDto, bindingResult);
        validateTypeOfCocktail(cocktailDto, bindingResult);
    }

    private void validateTypeOfCocktail(CocktailDto cocktailDto, BindingResult bindingResult) {
        try {
            TypeOfCocktail typeOfCocktail = TypeOfCocktail.valueOf(cocktailDto.getTypeOfCocktail());
        } catch (IllegalArgumentException exception) {
            FieldError fieldError = new FieldError("cocktailDto", "typeOfCocktail", "Invalid Type of Cocktail");
            bindingResult.addError(fieldError);
        }
    }

    private void validateIngredients(CocktailDto cocktailDto, BindingResult bindingResult) {
        if (cocktailDto.getName().length() == 0) {
            FieldError fieldError = new FieldError("cocktailDto", "ingredients", "Invalid ingredients");
            bindingResult.addError(fieldError);
        }
    }

    public void validateName(CocktailDto cocktailDto, BindingResult bindingResult) {
        if (cocktailDto.getName().length() == 0) {
            FieldError fieldError = new FieldError("cocktailDto", "name", "Invalid name");
            bindingResult.addError(fieldError);
        }
    }

    public void validatePrice(CocktailDto cocktailDto, BindingResult bindingResult) {
        try {
            Integer price = Integer.valueOf(cocktailDto.getPrice());
            if (price < 0) {
                FieldError fieldError = new FieldError("cocktailDto", "price", "Invalid price");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("cocktailDto", "price", "Invalid price");
            bindingResult.addError(fieldError);
        }
    }
}
