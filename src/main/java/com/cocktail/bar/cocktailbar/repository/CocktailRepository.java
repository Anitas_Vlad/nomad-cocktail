package com.cocktail.bar.cocktailbar.repository;

import com.cocktail.bar.cocktailbar.model.Cocktail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CocktailRepository extends JpaRepository<Cocktail, Integer> {

}
