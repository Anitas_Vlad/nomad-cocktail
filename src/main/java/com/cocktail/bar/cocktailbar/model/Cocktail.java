package com.cocktail.bar.cocktailbar.model;

import com.cocktail.bar.cocktailbar.model.enums.TypeOfCocktail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
public class Cocktail {
    @Id
    @GeneratedValue
    private Integer cocktailId;

    private String name;
    private Integer price;
    private String ingredients;

    @Enumerated(EnumType.STRING)
    private TypeOfCocktail typeOfCocktail;

    @ManyToMany
    @JoinTable(joinColumns = @JoinColumn(name = "cocktailId"), inverseJoinColumns = @JoinColumn(name = "wishlistId"))
    private List<WishList> wishlists;

}
