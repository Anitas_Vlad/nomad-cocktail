package com.cocktail.bar.cocktailbar.model.enums;

public enum Role {
    ADMIN, CLIENT
}
