package com.cocktail.bar.cocktailbar.model.enums;

public enum TypeOfCocktail {
    ALCOHOLIC, NON_ALCOHOLIC
}
