package com.cocktail.bar.cocktailbar.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
public class WishList {
    @Id
    @GeneratedValue
    private Integer wishlistId;

    @OneToOne
    private User user;

    @ManyToMany(mappedBy = "wishlists")
    private List<Cocktail> cocktails;
}
