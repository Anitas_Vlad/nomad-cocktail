package com.cocktail.bar.cocktailbar.model;

import com.cocktail.bar.cocktailbar.model.enums.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
public class User {
    @Id
    @GeneratedValue
    private Integer userId;

    private String name;
    private String email;
    private String password;
    private String phoneNumber;
    private String address;
    private Integer numberOfPeople;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}
