package com.cocktail.bar.cocktailbar.controllers;

import com.cocktail.bar.cocktailbar.model.Cocktail;
import com.cocktail.bar.cocktailbar.service.CocktailService;
import com.cocktail.bar.cocktailbar.validate.CocktailDtoValidator;
import dto.CocktailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.Binding;

@Controller
public class MainController {

    @Autowired
    private CocktailService cocktailService;
    @Autowired
    private CocktailDtoValidator cocktailDtoValidator;

    @GetMapping(value = "/add-cocktail")
    public String addCocktailGet(Model model, @ModelAttribute("addCocktailMessage") String addCocktailMessage){
        CocktailDto cocktailDto = new CocktailDto();
        model.addAttribute("cocktailDto", cocktailDto);
        model.addAttribute("addCocktailMessage", addCocktailMessage);
        return "add-cocktail";
    }

    @PostMapping(value = "/add-cocktail")
    public String addCocktailPost(@ModelAttribute(value = "cocktailDto") CocktailDto cocktailDto, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        cocktailDtoValidator.validate(cocktailDto, bindingResult);
        if (bindingResult.hasErrors()){
            return "add-cocktail";
        }
        cocktailService.addCocktail(cocktailDto);
        redirectAttributes.addFlashAttribute("addCocktailMessage", "The cocktail was added successfully");
        System.out.println(cocktailDto);
        return "redirect:/add-cocktail";
    }
}
