package com.cocktail.bar.cocktailbar.service;

import com.cocktail.bar.cocktailbar.mapper.CocktailMapper;
import com.cocktail.bar.cocktailbar.model.Cocktail;
import com.cocktail.bar.cocktailbar.repository.CocktailRepository;
import dto.CocktailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CocktailService {

    @Autowired
    private CocktailRepository cocktailRepository;

    @Autowired
    private CocktailMapper cocktailMapper;

    public void addCocktail(CocktailDto cocktailDto){
        Cocktail cocktail = cocktailMapper.mapCocktail(cocktailDto);
        cocktailRepository.save(cocktail);
    }
}
