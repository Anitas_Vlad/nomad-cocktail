package com.cocktail.bar.cocktailbar.mapper;

import com.cocktail.bar.cocktailbar.model.Cocktail;
import dto.CocktailDto;
import org.springframework.stereotype.Component;

@Component
public class CocktailMapper {

    public Cocktail mapCocktail(CocktailDto cocktailDto){
        Cocktail cocktail = new Cocktail();
        cocktail.setName(cocktailDto.getName());
        cocktail.setPrice(Integer.parseInt(cocktailDto.getPrice()));
        cocktail.setIngredients(cocktailDto.getIngredients());
        return cocktail;
    }

    public CocktailDto mapCocktailDto(Cocktail cocktail){
        CocktailDto cocktailDto = new CocktailDto();
        cocktailDto.setName(cocktail.getName());
        cocktailDto.setPrice(cocktailDto.getPrice());
        cocktailDto.setIngredients(cocktailDto.getIngredients());
        return cocktailDto;
    }
}
