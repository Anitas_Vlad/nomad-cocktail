package dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CocktailDto {

    private String name;
    private String price;
    private String ingredients;
    private String typeOfCocktail;
}
